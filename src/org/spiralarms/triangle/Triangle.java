package org.spiralarms.triangle;


import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Triangle implements Comparable<Triangle>, Iterator<Double> {
    double a, b, angle;
    int iteratorNextResult = 0;
    private static final Pattern triangleToStringRegex = Pattern.compile("Triangle\\{a=(\\d*\\.?\\d*),\\sb=(\\d*\\.?\\d*),\\sangle=(\\d*\\.?\\d*)}");

    public Triangle(double a, double b, double angleRadians) throws TriangleHasIncorrectDimensionsException {
        this.a = a;
        this.b = b;
        this.angle = angleRadians;

        validateDimensions();
    }

    public Triangle(String str) {
        Matcher m = triangleToStringRegex.matcher(str);
        if (m.find()) {
            this.a = Double.parseDouble(m.group(1));
            this.b = Double.parseDouble(m.group(2));
            this.angle = Double.parseDouble(m.group(3));
            validateDimensions();
        } else {
            throw new TriangleIncorrectStringException();
        }
    }

    protected void validateDimensions() {
        if (!(a > 0 && b > 0 && angle > 0 && angle < Math.PI))
            throw new TriangleHasIncorrectDimensionsException();
    }

    public double getOppositeSide() {
        return Math.sqrt(a * a + b * b - 2 * a * b * Math.cos(angle));
    }

    public double getArea() {
        return 0.5 * a * b * Math.sin(angle);
    }

    public double getPerimeter() {
        return a + b + getOppositeSide();
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", angle=" + angle +
                '}';
    }

    @Override
    public int compareTo(Triangle triangle) {
        if (triangle == this) return 0;
        return Double.compare(this.getArea(), triangle.getArea());
    }

    public double getAngle() {
        return angle;
    }

    @Override
    public boolean hasNext() {
        return iteratorNextResult <= 2;
    }

    @Override
    public Double next() {
        switch (iteratorNextResult++) {
            case 0:
                return a;
            case 1:
                return b;
            case 2:
                return angle;
            default:
                throw new NoSuchElementException();
        }
    }
}
