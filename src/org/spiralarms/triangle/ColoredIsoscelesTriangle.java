package org.spiralarms.triangle;

public class ColoredIsoscelesTriangle extends Triangle {
    public ColoredIsoscelesTriangle(double a, double angle, Color borderColor, Color fillColor) throws TriangleHasIncorrectDimensionsException {
        super(a, a, angle);
        this.borderColor = borderColor;
        this.fillColor = fillColor;
    }

    public ColoredIsoscelesTriangle(String str, Color borderColor, Color fillColor) {
        super(str);
        this.borderColor = borderColor;
        this.fillColor = fillColor;
    }

    private Color borderColor;
    private Color fillColor;

    public double getSide() {
        return a;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    @Override
    public String toString() {
        return super.toString() + "{borderColor=" + borderColor + ", fillColor=" + fillColor + "}";
    }
}
