package org.spiralarms.triangle;

import java.util.Comparator;

public class TriangleAngleCompare implements Comparator<Triangle> {
    @Override
    public int compare(Triangle t1, Triangle t2) {
        return Double.compare(t1.getAngle(), t2.getAngle());
    }
}