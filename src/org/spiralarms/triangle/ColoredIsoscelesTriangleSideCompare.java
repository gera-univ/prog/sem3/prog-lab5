package org.spiralarms.triangle;

import java.util.Comparator;

public class ColoredIsoscelesTriangleSideCompare implements Comparator<ColoredIsoscelesTriangle> {
    @Override
    public int compare(ColoredIsoscelesTriangle t1, ColoredIsoscelesTriangle t2) {
        return Double.compare(t1.getSide(), t2.getSide());
    }
}
