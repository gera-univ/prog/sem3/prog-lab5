package org.spiralarms.triangle;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        final Color clRed = new Color(255, 0, 0, 255);
        ColoredIsoscelesTriangle[] triangles = {new ColoredIsoscelesTriangle(4, Math.PI / 2, clRed, clRed),
                new ColoredIsoscelesTriangle("Triangle{a=2.0, b=2.0, angle=0.7853981633974483}", clRed, clRed),
                new ColoredIsoscelesTriangle(3.5, 2 * Math.PI / 3, new Color(0, 0, 255, 255), clRed)};
        System.out.println("Before sorting\n" + Arrays.deepToString(triangles));
        ColoredIsoscelesTriangleSideCompare sideCmp = new ColoredIsoscelesTriangleSideCompare();
        Arrays.sort(triangles, sideCmp);
        System.out.println("After sorting\n" + Arrays.deepToString(triangles));

        for (int i = 0; i < triangles.length; ++i) {
            System.out.println("Iterating over the " + i + " element of the array");
            Triangle subject = triangles[i];
            while (subject.hasNext()) {
                System.out.println(subject.next());
            }

            System.out.printf("It's area, opposite side and perimeter are %s, %s and %s respectively\n", subject.getArea(), subject.getOppositeSide(), subject.getPerimeter());
        }
    }
}
