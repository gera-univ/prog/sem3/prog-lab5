package org.spiralarms.triangle;

public class Color {
    int red;
    int green;
    int blue;
    int alpha;

    public Color(int red, int green, int blue, int alpha) throws IncorrectColorException {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;

        validateColor();
    }

    private void validateColor() throws IncorrectColorException {
        if (!(red >= 0 && red <= 255 && green >= 0 && green <= 255 && blue >= 0 && blue <= 255 && alpha >= 0 && alpha <= 255))
            throw new IncorrectColorException();
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    @Override
    public String toString() {
        return "(" + red + ", " + green + ", " + blue + ", " + alpha + ")";
    }
}
