package org.spiralarms.triangle;

import java.util.Comparator;

public class TrianglePerimeterCompare implements Comparator<Triangle> {
    @Override
    public int compare(Triangle t1, Triangle t2) {
        return Double.compare(t1.getPerimeter(), t2.getPerimeter());
    }
}